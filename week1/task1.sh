#!/bin/bash

# Commands and vars
imp=("~/*" "~/.ssh" "/tmp" "/dev/shm" "/ur/local")
files_by_create_date=`ls -lrth`
root_ps=`ps -aux | grep "root"`


# DIR enum
dir_enum (){
	echo -e "\n########## Important DIRs ##########\n"
	for d in "${imp[@]}";do
		if [[ -d "$d" ]]; then
			echo "`ls -la $d`"
		fi
	done
}

# Usage: arithmetic operator num1 num2
# arithmetic + 1 2
arithmetic (){
	echo -e "\n########## Arithmetic ##########\n"
	echo $(expr $2 $1 $3)
}

main (){
	echo -e "\nScript started at `date`\n"

	echo -e "\n########## Running services ##########\n"
	echo "`ps -aux`"
	dir_enum
	arithmetic + 1 2
}
main
# Backup DIR
mkdir backup



