# Docs of Task 1
The procedures explored in `task1.md`.

### Concepts explained:
- **Piping**:    
    In Unix systems, the pipe operator `|` connects commands.    
    It passes the result of the command to the left of the operator, and uses it as input for the command on its right.
- **Variables**:    
    In bash, variables are the same as in any programming language.    
    They are values stored in memory. The difference is, is that in bash you can assign a command to a variable. For example:    
    ```bash
    root_process=`ps aux | grep "root"`
    ```
    In my case, when running the variable `root_process` we would see the following:    
    ```bash
    └─$ ps aux | grep "root"
    root         1  0.0  0.0   8940   324 ?        Ssl  08:54   0:00 /init
    root         7  0.0  0.0   8940   228 tty1     Ss   08:54   0:00 /init
    hackerm+    48  0.0  0.0  14460  1228 tty1     S    09:31   0:00 grep --color=auto root
    ```
- **Redirection**: The concept of redirecting in bash is very simple.    You take the result of an operation or command 
    and use the redirect operators `>`, `>>`, `<` to write to a file, or take the content of a file and parse it into the command used with the redirect operator.
- **Special characters**:    
    These are characters that are used within the language and are interpreted (by the lexer I would suppose) as not part of a string, as they fill in a different role in the language.    For example: pipe character `|`, redirect operator(s) `>`, negating character`!`    
    There are characters such as the  new line `\n` and carriage return `\r` characters. These special characters are used to move and separate strings.     
    For example, the `\n` character makes a new line wherever it is typed.    
    The characters using the `\` character can be *escaped* to be evaluated and used as it was intended, as opposed to part of the string. See the example:    
    ```bash
    └─$ echo "test\n"
    test\n

    └─$ echo -e "test\n"
    test
    ```
    As you can see, using the `-e`  flag with the echo command will evaluate the character instead of treating it as part of the string.
- **Arithmetic operations**:   
    These kind of operations can be used in bash the following way: `expr 1 + 2`.    
    They can even be assigned to a variable like: `res=$(expr 1 + 2)`    
    The `expr` keyword / command is used to evaluate the expression following the command.    
    Note the `$`, it is used to return the result from the expression, not just evaluate it.
    ```bash
    └─$ res=$(expr 1+2)

    ┌──(hackerman㉿HackerMan)-[/mnt/c/users/krist/onedrive/desktop/bsc_cs/c/tasks/task_1]
    └─$ echo $res
    1+2
    ```

### Common command && Commands used:
- `ps -aux`:    
    This command is used to show all running processes for all the users.
    Piping this command with `grep "root"`, will show us all of the processes ran by root.
- `ls -lrth`
    The main command `ls` shows all the files available in the pwd.
    The `lrth` flag is used to list those file, by creation date.
- `curl`: Used for making web requests to a server. It can send cookies and different types of requests.    
    If a site has an endpoint or the user would just like to download the content of the site, they could use the  flag `-o`    
    ```bash
    curl kristofhracza.com -o mysite.html

    curl https://icanhazip.com

    curl -X POST -F "name=username" -F "pass=password" https://site.com/login.php
    ```
    In the last example the `-F` flag is used to send HTML form data to a site.    
- `mkdir` && `rmdir`: This command is used to make a new directory.     Can be used like : `mdkir newdir`   
    If the user would like to delete the directory, they can use the `rmdir` command.    
    Just type `rmdir` and the name of the directory to be deleted.    
    If the folder is not empty the user can use the `-rf` flag, to remove all the content within the folder recursively.
    
### Directories:
In the project file, there's an array which looks like this: 
```bash
imp=("~/*" "~/.ssh" "/tmp" "/dev/shm" "/ur/local")
```
All of these directories are then enumerated by a for loop to check for any files that could be useful.